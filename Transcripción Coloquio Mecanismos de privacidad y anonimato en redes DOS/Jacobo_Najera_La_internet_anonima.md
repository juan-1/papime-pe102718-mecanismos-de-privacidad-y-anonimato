## Jacobo Nájera, La internet anónima: Tor en México ##

La red Tor en México

Juan Jacobo Nájera Valdez


Un grupo vinculado a los derechos humanos y al software libre en 2016 se preguntaron  “cuál es la participación de América Latina en la red Tor”, tanto en su demografía de usuarios como con infraestructura y desarrollo de la misma. El presente texto es un seguimiento a los hallazgos de este grupo y los alcances de las acciones emprendidas para alentar la participación desde México.

Una de las primeras respuesta que obtuvo el grupo se encontró en las métricas propias del proyecto, resultado de mediciones internas del uso de la red Tor. La respuesta consta de 12445 usuarios en México de un total de 2 millones de la red, diarios. (metrics, noviembre 2018).

La red Tor se conforma por más de 6000 nodos (metrics, enero 2019), donde cada nodo tiene diferentes funciones. Desde la perspectiva de su arquitectura, la red tiene 11 autoridades centrales y repetidores conocidos como nodos que de acuerdo a su configuración y características técnicas pueden ser de cuatro tipos: puentes, entrada, intermedio y salida. 		

La red Tor por diseño tiene el objetivo de brindar anonimato a capa de red. Esto significa que quienes se conectan por medio de ella pasan por diferentes nodos, lo que produce en resultado que el destinatario final de la comunicación no pueda obtener la dirección IP de origen, con lo que se logra una capa de anonimato a nivel red.

El grupo de interés  antes mencionado con integrantes diversos desde investigadores independientes, estudiantes y organizaciones de la sociedad civil, fue articulado en el proyecto Mecanismos de Privacidad y Anonimato de la  Universidad Nacional Autónoma de México (Aurelio P., 2017)

Proyecto que plantea dos ejes de trabajo, por un lado la divulgación de las herramientas enfocadas en la privacidad y el anonimato en el contexto de la enseñanza a nivel superior, y por otra parte la realización de tareas encaminadas para facilitar la participar en el desarrollo de las red Tor, desde México (UNAM-DGAPA-PAPIME PE-102718).

Una vez que fue de conocimiento común para el grupo de trabajo que México tiene un promedio de 12 mil usuarios, también se quería saber cuál es la participación activa en la construcción de la infraestructura de la red desde el país. En otras palabras, conocer cuántos nodos del promedio de los 6 mil que conforman la red están geográficamente distribuidos en México.

En ese momento se encontró un par de nodos intermedios y de entrada, y ninguno de salida. Cifra que visibiliza una participación porcentual del  .03 %. desde México. Para poner en contexto el porcentaje, de un total de 1.5% de nodos en  América Latina.
Una participación tremendamente baja tanto en las y los usuarios, y la operación de nodos en perspectiva en toda la región latinoamericana y el Caribe.

Los datos permiten plantear otras preguntas, además de la inicial,  sobre los motivos o explicaciones posible que dan como resultado esos porcentaje. Como también el papel actual y potencial que juega una arquitecturas de red con estas características.

El enfoque que toma entonces el grupo en México se concentra en entender los motivos de esa baja participación en el desarrollo de la infraestructura para buscar incentivar su crecimiento o fortalecimiento de la red de anonimato más conocida en el mundo.

Las principales formas de conectividad en México son: ADSL, Internet por Cable, fibra óptica, redes de telefonía móvil y conexión vía satélite. Con el acceso a Internet en los hogares de 50.9 % según la ITU (2017).

Es importante señalar un fenómeno con los operadores de nodos. Ya que hay operadores desde México de nodos, pero con infraestructura en otros países. Cuando se indagaba sobre los motivos para tener la infraestructura en otros países y no en México a los operadores, la respuesta fueron por motivos de costo.  



Algunos de los operados suelen rentar servidores virtuales en servicios de otros países y ahí instalar el software para tener sus nodos. Esto por una parte indica interés en la participación para la construcción de la infraestructura de la red, pero limitaciones contextuales más precisamente el costo, según los testimonios de los operadores.

Las principales limitaciones técnicas que plantean los proveedores de telecomunicaciones en México para la instalación de nodos que puedan ser parte de la red Tor son dos: La primera es una medida del bloqueo por parte de Telmex de 9 de 11 de las direcciones IP que pertenecen a los nodos directorio de la red Tor, lo que en consecuencia no permite la sincronización de los nodos y por lo tanto no es posible la instalación.

La segunda medida está asociada a las formas de proveeduría más comunes sobre todos en  los estados, al implementar técnicas de resolución de direcciones IP por NAT. Técnica que en términos más coloquiales significa una constante práctica en la que el proveedor de telecomunicaciones asigna una dirección IP a varios de sus clientes o abonados, en algunos de los casos cientos o miles. Esta técnica del NAT no permite que el nodo pueda tener una dirección pública única asignada al mismo.

NAT es una técnica que ocupan algunos administradores y que consiste en “un método por el que muchas direcciones de red y sus puertos TCP/UDP (Protocolo de Control de Transmisión/Protocolo de Datagrama de Usuario) son traducidos a una sola dirección de red y sus puertos TCP/UDP”.

Anterior definición que se recoge para fines informativos en el RFC número 3022, por Network Working Group del [Internet Engineering Task Force](https://www.ietf.org/) o en español Grupo de Trabajo de Ingeniería de Internet. Espacio donde se desarrollan estándares de las normas técnicas de Internet y sus protocolos.  

Uno de los argumentos que muestran los proveedores de conectividad para la utilización de NAT en la gestión de su red es como medida para  hacer frente al agotamiento de direcciones IPv4, como método de racionalización de los recursos disponibles.

En el proyecto de la Universidad Nacional Autónoma de México “Mecanismos de Privacidad y Anonimato” se comenzó un llamado para realizar un censo que permitió hasta el momento tener hallazgos preliminares de las limitaciones generales que hay en los proveedores mexicanos. Detectando con ello el bloqueo a nivel ruta de las siguientes direcciones IP en la red de Telmex:

*	171.25.193.9
*	86.59.21.38
*	199.58.81.140
*	194.109.206.212  
*	131.188.40.189
*	128.31.0.34
*	193.23.244.244
*	154.35.175.225
*	128.31.0.39

El bloqueo de los 9 servidores  del sistema de autoridades de directorio (Directory Authority System) es uno de los hallazgos de los interesados en participar voluntariamente en la red Tor desde México, como un obstáculo para la instalación de nodos.

Una medida que  significa un retraso  para la plena participación de espacios de investigación y de la ciudadanía en la red para el ejercicio del derecho al anonimato en Internet.


En la práctica  los grupos interesados en elevar la participación en la operación de nodos de Tor encuentran buena recepción por parte de personas vinculadas al software libre y los derechos humanos. Pero en el momento de dar el siguiente paso de la instalación de un nodo de entrada o intermedio por lo regular, sucede que no es posible por alguna de estos dos medidas o limitaciones técnicas, tanto por la manera del despliegue por NAT como el bloqueo por parte de Telmex. Y en muchos de los casos no es posible que encuentren o contraten otro proveedor.



En los casos en los que se logra sortear las limitaciones técnicas y que es una de las inquietudes sino es que es la más común cuando una persona está interesada en la instalación de un nodo es sobre las implicaciones legales que puede conllevar la operación de un nodo.


En México la operación es legal, “bajo el entendido que un nodo de la red Tor no es un concesionario de Internet o un proveedor de interconexión pública, sino una conexión privada gratuita que la persona usuaria acepta al momento de acceder a la red Tor, la cual se realiza en el ejercicio de los derechos y libertades de privacidad, acceso a Internet, intercambio de información, libertad de expresión y manifestación de ideas que otorgan el artículo 6º Constitucional y el artículo 191 fracción XV de la Ley Federal de Telecomunicaciones y Radiodifusión”.  Como lo recoge Enjambre Digital, una de las organizaciones participantes del proyecto de Mecanismos de Privacidad y Anonimato

Los servicios en Internet son parte de lo que en la jerga de las regulaciones  de telecomunicaciones, se llaman intermediarios.  En esa dirección la discusión que se ha dado en torno a las responsabilidades  que tiene un intermediario y en especial un servicio que funciona sobre Internet como lo puede ser un sitio web, correo electrónico, entre otros son de carácter vigente y pueden variar al estar ligadas a la etnografía de las regulaciones.

Desde organizaciones de la sociedad civil se ve reflejado en  los llamados Principios de Manila. Uno de los eslabones detrás de los principios es el entendido que el conjunto de prácticas, regulaciones y  la responsabilidad de los intermediarios tienen una vinculación e impacto en el ejercicio de los derechos humanos de quienes usan las plataformas de Internet, como lo es la libertad de expresión, asociación y la privacidad.

Con estos principios busca contribuir al desarrollo de marcos de responsabilidad de intermediarios  “que puedan promover la innovación, y, a la vez, respeten los derechos de los usuarios consagrados en la Declaración Universal de Derechos Humanos, el Pacto Internacional de Derechos Civiles y Políticos y los Principios Rectores de Naciones Unidas sobre las Empresas y los Derechos Humanos”

Estos  6 principios contemplan

1.	Los intermediarios deberían estar protegidos por ley de la responsabilidad por contenido de terceros.
2.	No debe requerirse la restricción de contenidos sin una orden emitida por una autoridad judicial
3.	Las solicitudes de restricción de contenidos deben ser claras, inequívocas y respetar el debido proceso
4.	Las leyes, órdenes y prácticas de restricción de  contenidos deben cumplir con los tests  de necesidad y  proporcionalidad
5.	Las leyes, políticas y prácticas de restricción de contenido deben respetar el debido proceso
6.	La transparencia y la rendición de cuentas deben ser incluidas dentro de la normativa, políticas y prácticas sobre restricción de contenido


Estos 6 principios cuentan con especificidades y criterios que los acompañan y que pueden ser consultados en línea. Sin embargo el motivo de incluir estos principios es compartir parte de uno de los marcos que destacan desde sociedad civil como ejercicio de discusión informado actual de las públicas y regulaciones de los intermediarios de Internet.

Una vez realizado parte de la revisión del contexto de algunas de las discusiones vinculadas a los intermediarios y el papel que pueden tener sus políticas, desde la perspectiva de organizaciones de la sociedad civil que trabajan los derechos digitales, y concluir que el marco normativo legal en México es compatible con la operación de nodos de la red Tor.

Por lo regular los tipos de nodos que están más propensos a recibir requerimientos legales son los de salida. Esto se debe a que son el último contacto entre la red Tor y otras redes de Internet. Para ello hay prácticas recomendadas para la operación de los nodos de salida.

La organización que se encargo del desarrollo y análisis de las normativas que arribaron en el diseño de un aviso legal, fue la organización Enjambre Digital.  Este aviso legal tiene dos pilares, el primero es el anclaje de la declaratorio de responsabilidad desde la arquitectura propia de la red Tor sobre posibles requerimientos de información por autoridades:

> “La responsabilidad respecto a  los sitios finales visitados corresponde únicamente a quienes hacen uso de Internet. Como parte intrínseca del proceso para preservar el anonimato, este nodo no conserva alguna clase de registro sobre: páginas visitadas, medios de conexión, paquetes de datos que cruzan por él o algún otro dato que pueda identificar a las personas que lo utilizan.”

Finamente el segundo pilar del aviso es el fundamento legal sobre el ejercicio de derecho a la privacidad, acceso a internet, intercambio de información y libertad de expresión que otorga el  el artículo 6o Constitucional y el artículo 191 fracción XV de la Ley Federal de Telecomunicaciones y Radiodifusión en México.

Finalmente tras los esfuerzos de los diversos actores involucrados que convergieron en el proyecto de Mecanismos de nonimato y privacidad de la Universidad Nacional Autónoma de México se logra el incremento de la participación en la operación de nodos con 8 nodos más, incluyendo uno de salida.

Por lo antes expuesto nos permite concluir que la participación de los y las interesadas en la operación de nodos de la red Tor presentan dificultades por las características técnicas de las mayores proveedores de acceso a internet por el bloqueo de las 8 direcciones IP, y en otros casos por la arquitectura de proveeduría implementación de NAT.

No obstante se reconocen los esfuerzos multidisciplina para sortear las limitaciones técnicas y  de política de los telcos antes descritas y así lograr un incremento en la participación de México en la red Tor. En un contexto que se observa con tendencia restrictiva.

Uno de los pasos siguientes es la exploración de los marcos regulatorios que atiendan en diferentes dimensiones, entre ellos de competencia, neutralidad de la red y libertad de expresión y derechos de los consumidores. Para así lograr una plena participación de quienes voluntariamente desean contribuir con su infraestructura existe al funcionamiento de la Tor Network.

## Referencias ##

Aviso legal para nodos de salida en México
https://www.enjambre.net/tor-mx/


Colabora en la documentación de las barreras para el despliegue de la red Tor en México
https://tor.enjambre.net/convocatoria/

Principios De Manila Sobre Responsabilidad De Los Intermediario (2015) https://www.eff.org/files/2015/06/23/manila_principles_1.0_es.pdf

Mecanismos de privacidad y anonimato en redes. LIDSOL crea proyecto multidisciplinario para protección de derecho en la red. (2017) Aurelio Pérez-Gómez
http://www.comunicacionfi.unam.mx/mostrar_nota.php?id_noticia=1060

Privacy-preserving monitoring of an anonymity,network
https://iain.learmonth.me/content/talks/2019/2019-02-fosdem.pdf

Tor Metrics, servers
https://metrics.torproject.org/networksize.html

Consulta de usuario AT&T Internet en casa Tipo de NAT
https://forums.att.com/t5/Servicios/AT-amp-T-Internet-en-casa-Tipo-de-NAT/td-p/5508834

Finally, a sensible increase in participation for Tor in Mexico!
https://gwolf.org/node/4135

La Internet anónima: Tor en México
https://tor.enjambre.net/

Tor Metrics
https://metrics.torproject.org/

Mexico Profile (Latest data available: 2018)
https://www.itu.int/net4/itu-d/icteye/CountryProfileReport.aspx?countryID=147

Proyecto Mecanismos de Privacidad y Anonimato
https://www.priv-anon.unam.mx/

Traductor de Dirección de Red IP Tradicional (NAT Tradicional)
https://www.rfc-es.org/rfc/rfc3022-es.txt






